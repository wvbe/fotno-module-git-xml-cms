'use strict';

const express = require('express');

const bodyParser = require('body-parser');

const cmsRoutes = [
	require('./cms/GET-document'),
	require('./cms/POST-document'),
	require('./cms/PUT-document'),
	require('./cms/PUT-document-lock'),
	require('./cms/GET-heartbeat'),
	require('./cms/POST-browse'),
	require('./cms/POST-reference-create'),
	require('./cms/POST-reference-get')
];
const publishRoutes = [
	require('./publish/GET-document'),
	require('./publish/POST-search')
];

module.exports = (config, router, repo) => {
	const cmsRouter = express.Router(),
		publishRouter = express.Router();

	cmsRouter.use('/', bodyParser.json({
		limit: '5mb'
	}));

	cmsRoutes.forEach(route => route(config, cmsRouter, repo));
	publishRoutes.forEach(route => route(config, publishRouter, repo));

	router.use('/connectors/cms/standard', cmsRouter);
	router.use('/connectors/publish', publishRouter);
};
