'use strict';

const crypto = require('crypto');
const fs = require('../../repository');

module.exports = (config, router, repo) => {
	return router
		.route('/document')
		.post((req, res) => {
			const file = repo.createFile('document', req.body.content, req.body.folderId);

			res
				.status(201)
				.json({
					documentId: file.getId(),
					lock: file.getLockStatus(),
					metadata: file.getMetadata(),
					content: file.getXml()
				});
		});
};
