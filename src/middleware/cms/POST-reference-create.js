'use strict';

const path = require('path');
const fs = require('../../repository');

module.exports = (config, router, repo) => {
	return router
		.route('/reference/create')
		.post((req, res) => {
			const reference = fs.createReference(req.body.type, req.body.target, req.body.metadata);

			res
				.status(201)
				.json(reference.getObject());
		});
};
