'use strict';

const crypto = require('crypto');
const fs = require('../../repository');

module.exports = (config, router, repo) => {
	return router
		.route('/reference/get')
		.post((req, res) => {
			res
				.status(200)
				.json({
					results: req.body.permanentIds.map(permanentId => {
						const reference = fs.getReference(permanentId);
						try {
							return {
								status: reference ? 200 : 404,
								body: reference.getObject()
							};
						} catch (e) {
							return {
								status: 404
							}
						}
					})
				});
		});
};
