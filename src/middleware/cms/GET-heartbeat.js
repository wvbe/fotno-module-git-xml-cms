'use strict';

const crypto = require('crypto');

module.exports = (config, router, repo) => {
	router
		.route('/heartbeat')
		.get((req, res) => {
			res.sendStatus(204);
		});
};
