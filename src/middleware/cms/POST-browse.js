'use strict';
const path = require('path');
const crypto = require('crypto');

const getAllStatsInPath = require('../../util/getAllStatsInPath');
const getStatForPath = require('../../util/getStatForPath');

module.exports = (config, router, repo) => {
	return router
		.route('/browse')
		.post((req, res) => {
			const folderId = (req.body.folderId || ''),
				assetTypes = req.body.assetTypes,
				resultTypes = req.body.resultTypes,
				dir = path.resolve(config.repo, folderId),
				scan = getAllStatsInPath(dir);

			const results = scan
				.filter(stat => path.basename(stat.path).charAt(0) !== '.')
				.map(stat => {
					stat.id = path.relative(config.repo, stat.path);

					if (stat.file)
						stat.fileInst = repo.getFile(stat.id);

					return stat;
				})
				.filter(stat => {
					if (stat.directory)
						return resultTypes.includes('folder');

					if (!resultTypes.includes('file'))
						return false;

					if (stat.fileInst.getClass() !== 'asset')
						return false;

					if (!assetTypes.includes(stat.fileInst.getType()))
						return false;

					return true;
				})
				.map(stat => {
					if (stat.directory)
						return {
							id: stat.id,
							type: 'folder',
							label: path.basename(stat.path),
							metadata: {
								icon: 'folder-o'
							}
						};

					return {
						id: stat.id,
						type: stat.fileInst.getType(),
						label: stat.fileInst.getLabel(),
						metadata: stat.fileInst.getMetadata()
					};
				})
				.sort((a, b) => a.label.localeCompare(b.label));

			res.json({
				totalItemCount: results.length,
				items: results,
				metadata: {
					hierarchy: folderId.split('/')
						.map((segment, i) => ({ id: folderId.split('/').slice(0, i).join('/'), label: segment || 'Root' }))
				}
			});

		});
};
