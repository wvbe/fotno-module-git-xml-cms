'use strict';

const crypto = require('crypto');

module.exports = (config, router, repo) => {
	router
		.route('/document')
		.get((req, res) => {
			const file = repo.getFile(req.query.documentId);

			if (!file.exists()) {
				return res.sendStatus(404);
			}

			res.json({
				documentId: file.getId(),
				lock: file.getLockStatus(),
				metadata: file.getMetadata(),
				content: file.getXml()
			});
		});
};
