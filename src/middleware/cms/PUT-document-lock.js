'use strict';

module.exports = (config, router, repo) => {
	return router
		.route('/document/lock')
		.put((req, res) => {
			res.sendStatus(204);
		});
};
