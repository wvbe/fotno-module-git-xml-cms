'use strict';

const crypto = require('crypto');
const fs = require('../../repository');

module.exports = (config, router, repo) => {
	return router
		.route('/document')
		.put((req, res) => {
			const file = repo.getFile(req.body.documentId);

			if (!file.exists()) {
				return res.sendStatus(404);
			}

			file.write(req.body.content);

			res.sendStatus(204);
		});
};
