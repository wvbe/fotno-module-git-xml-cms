'use strict';

const getPublicDocumentObject = require('./getPublicDocumentObject');

module.exports = (config, router, repo) => {
	router
		.route('/document')
		.get((req, res) => {
			const file = repo.getFile(req.query.documentId);

			if (!file.exists()) {
				return res.sendStatus(404);
			}

			res.json(getPublicDocumentObject(file));
		});
};
