'use strict';
const path = require('path');
const search = require('../../api/search');
const getPublicDocumentObject = require('./getPublicDocumentObject');

// Promisified instantiation of search-index

module.exports = (config, router, repo) => {
	router
		.route('/search')
		.get((req, res) => {
			console.dir(req.body);
			console.dir(req.query);
			console.dir(req.params);

			const query = req.query.query;
			return search.getResults(query)
				.then(files => {
					res.json({
						query: query,
						total: files.length,
						results: files.map(file => getPublicDocumentObject(file))
					});
				})
				.catch(err => {
					console.log(err.stack);
					res.sendStatus(500);
				});
		});
};
