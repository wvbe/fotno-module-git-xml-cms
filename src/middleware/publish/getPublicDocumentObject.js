const fontoxpath = require('fontoxpath');
const repository = require('../../repository');
const stringToDom = require('../../util/stringToDom'),
	jsonmlToString = require('../../util/jsonmlToString'),
	filterDom = require('../../util/filterDom'),
	domToJsonml = require('../../util/domToJsonml');

module.exports = function getPublicDocumentObject (file) {
	let dom = stringToDom(file.getXml(true));

	dom = filterDom(
		dom.documentElement,
		node => {
			if (!node.nodeName)
				return false;

			if (node.nodeName === 'draft-comment')
				return false;

			return true;
		});

	fontoxpath.evaluateXPathToNodes('//*[@href]', dom).forEach(node => {
		const referenceId = node.getAttribute('href'),
			reference = repository.getReference(referenceId),
			target = reference.getObject().target;

		node.setAttribute('href', target);
	});

	const xml = jsonmlToString(domToJsonml(dom));

	return {
		id: file.getId(),
		title: file.getLabel(),
		dom: xml
	};
}
