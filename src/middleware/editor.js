'use strict';

const path = require('path');
const express = require('express');

module.exports = (config, router, repo) => {
	router.use('/', express.static(config.editor));
};
