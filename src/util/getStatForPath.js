
const fs = require('fs'),
	path = require('path');

function getStatForPath($p, indiscriminateOfSymlinks) {
	const stat = Object.assign(
		fs[indiscriminateOfSymlinks
			? 'statSync'
			: 'lstatSync']($p),
		{ path: $p });

	return {
		path: stat.path,
		symlink: stat.isSymbolicLink()
			? path.resolve(path.dirname(stat.path), fs.readlinkSync(stat.path))
			: false,
		file: stat.isFile(),
		directory: stat.isDirectory(),
		size: stat.size,
		accessed: stat.atime,
		modified: stat.mtime,
		changed: stat.ctime,
		created: stat.birthtime
	};
}

module.exports = getStatForPath;
