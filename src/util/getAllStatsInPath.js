
const fs = require('fs'),
	path = require('path');

const getStatForPath = require('./getStatForPath');

function getAllStatsInPath (dir, indiscriminateOfSymlinks) {
	return fs.readdirSync(dir)
		.map(file => path.resolve(dir, file))
		.map(path => {
			try {
				return getStatForPath(path, indiscriminateOfSymlinks)
			} catch (e) {}
		})
		.filter(result => !!result);
}

module.exports = getAllStatsInPath;
