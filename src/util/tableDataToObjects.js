module.exports = (tableData) => {

	const columns = tableData.substr(0, tableData.indexOf('\n')).split(','),
		nodesCsv = tableData
			.substr(tableData.indexOf('\n'))
			// Replace empty quoted columns with nothingness because they'll break after undoubling doubledouble quotes
			//   Potentially unsafe, as it also does replaces in extendedproperties
			.replace(/,"","","",/g, ',,,,')
			.replace(/,"","",/g, ',,,')
			.replace(/,"",/g, ',,')
			.replace(/""/g, '"'),
		objects = [];

	let i = 0,
		isInQuotedText = false,
		isInColumnNumber = 0,
		phrase = '',
		currentObject = [];

	while (i < nodesCsv.length) {
		const char = nodesCsv[i];

		if (char === '"') {
			// If (potentially) running into a quote-encapsulated column value
			if (columns[isInColumnNumber] === 'extendedproperties') {
				// The extendedprops column, special rules apply because the "middle layer" of quotes are not escaped
				if (!phrase && !isInQuotedText) {
					// Must be the start of an extended props
					isInQuotedText = !isInQuotedText;
				}
				else if (isInQuotedText && nodesCsv[i - 1] === '"' && nodesCsv[i - 2] !== '\\' && nodesCsv[i + 1] === ',') {
					// Must be the end of extended props
					isInQuotedText = !isInQuotedText;
				}
				else {
					// A quote within extendedprops is retained
					phrase += char;
				}
			}
			else {
				isInQuotedText = !isInQuotedText;
			}
		}

		else if ((char === ',' && !isInQuotedText) || (isInColumnNumber === columns.length - 1 && !isInQuotedText && char === '\n')) {
			// If running into the end of a column value, save phrase
			currentObject[isInColumnNumber] = phrase.trim();

			// Reset
			++isInColumnNumber;
			phrase = '';

			if (isInColumnNumber >= columns.length) {
				// If also running into the end of a row, save row
				objects.push(currentObject);

				// Reset
				currentObject = [];
				isInColumnNumber = 0;
			}
		}

		else {
			// Otherwise, character must be part of the column value we're currently traversing
			phrase += char;
		}

		++i;
	}

	return objects.map(obj => obj.reduce((proper, attr, i) => Object.assign(proper, { [columns[i]]: attr }), {}))
		.map(obj => {
			const json = '{' + obj.extendedproperties
					.replace(/\n/g, '\\n')
					.replace(/\t/g, '\\t')
					.replace(/=>/g, ':') + '}';
			try {
				obj.extendedproperties = JSON.parse(json);
			} catch (e) {
				console.log(e.message);
				console.log();
				console.log(json);
				console.log();
				console.log();
				console.log();
			}

			return obj;
		});
};

// @TODO: Sanitize returned objects further
// - unescaping double quotes in XML
