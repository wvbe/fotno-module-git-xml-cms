const DOMParser = require('xmldom').DOMParser,
	domParser = new DOMParser();

module.exports = function (str) {
	return domParser.parseFromString(str, 'application/xml');
};
