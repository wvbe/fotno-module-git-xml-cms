const nodeTypes = {
	element: 1,
	text: 3,
	pi: 7,
	comment: 8,
	doc: 9,
	doctype: 10,
	docfragment: 11
};

module.exports = function domToJsonMl (node) {
	if (node.nodeType === nodeTypes.text) {
		return node.nodeValue;
	}

	if (node.nodeType === nodeTypes.comment) {
		return node.data ? ['!', node.data] : ['!'];
	}

	if (node.nodeType === nodeTypes.pi) {
		return node.data ? ['?' + node.target, node.data] : ['?' + node.target];
	}

	if (node.nodeType === nodeTypes.doctype) {
		return ['!DOCTYPE', node.name, node.publicId, node.systemId];
	}

	// Serialize element
	const jsonml = [node.nodeName];

	if (node.attributes && node.attributes.length) {
		const attributes = {};

		for (let i = 0, l = node.attributes.length; i < l; ++i) {
			const attr = node.attributes[i];
			attributes[attr.name] = attr.value;
		}

		jsonml.push(attributes);
	}

	// Serialize child nodes
	for (let childNode = node.firstChild; childNode; childNode = childNode.nextSibling) {
		jsonml.push(domToJsonMl(childNode));
	}

	return jsonml;
}
