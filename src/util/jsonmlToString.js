module.exports = function jsonmlToString (jsonMl) {
	if (typeof jsonMl === 'string') {
		return jsonMl
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
	}

	let xmlString = '';

	let nodeName = jsonMl.shift();

	if (nodeName === '#document')
		nodeName = null;

	if (nodeName && nodeName.charAt(0) === '?') {
		return '<' + nodeName + (jsonMl.length ? ' ' + jsonMl.join('') : '') + '?>';
	}

	if (nodeName) {
		xmlString += '<' + nodeName;

		if (jsonMl[0] && typeof jsonMl[0] === 'object' && !Array.isArray(jsonMl[0])) {
			const attributes = jsonMl.shift();
			Object.keys(attributes).forEach(attributeName => {
				xmlString += ' ' + attributeName + '="' + attributes[attributeName] + '"';
			});
		}

		if (!jsonMl.length) {
			xmlString += ' />';
			return xmlString;
		}

		xmlString += '>';
	}

	while (jsonMl.length) {
		xmlString += jsonmlToString(jsonMl.shift());

	}

	if (nodeName)
		xmlString += '</' + nodeName + '>';

	return xmlString;
};

