module.exports = function filterDom (dom, filter) {
		(function traverse(child) {
			const include = filter(child);

			if (!include) {
				child.parentNode.removeChild(child);
			}
			if (include && child.firstChild) {
				traverse(child.firstChild);
			}
			if (child.nextSibling) {
				traverse(child.nextSibling);
			}
		})(dom);

		return dom;
	};
