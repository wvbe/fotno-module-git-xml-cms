const fs = require('fs-extra');
const path = require('path');


const assetTypes = require('../assetTypes');
const generateUniqueIdentifier = require('../util/generateUniqueIdentifier');

const CmsObject = require('./CmsObject');

module.exports = class File extends CmsObject {
	/**
	 *
	 * @returns {string}
	 */
	getLabel () {
		try {
			return JSON.parse(fs.readFileSync(this.getLocation(), this.getEncoding())).title || 'Untitled document';
		}
		catch (e) {
			const basenameParts = path.basename(this.getLocation()).split('.');
			return basenameParts.slice(1, basenameParts.length - 1);
		}
	}

	/**
	 * @param {boolean} forceRefresh - Bust file content cache if set to TRUE
	 * @returns {string} XML
	 */
	getXml (forceRefresh) {
		let input = super.read(forceRefresh);

		const assetType = assetTypes.get(this.getType());

		const serializer = assetType.serializer;
		if (serializer)
			input = serializer.deserialize(this, input);

		return input;
	}

	/**
	 * @returns {Object}
	 * @todo Implement
	 */
	getMetadata () {
		const assetType = assetTypes.get(this.getType());
		return {
			icon: assetType.icon
		};
	}

	/**
	 *
	 * @returns {{isLockAcquired: boolean, isLockAvailable: boolean}}
	 * @todo Implement
	 */
	getLockStatus () {
		return {
			isLockAcquired: true,
			isLockAvailable: true
		};
	}

	/**
	 *
	 * @param {string} xml
	 */
	write (xml) {
		const assetType = assetTypes.get(this.getType());
		const serializer = assetType.serializer;
		let input = xml;

		if (serializer)
			input = serializer.serialize(this, input);

		return super.write(input);
	}

	/**
	 *
	 * @param {string} type
	 * @param {string} xml
	 * @param {string} folder
	 * @returns {File}
	 */
	static create (type, xml, folder) {
		const file = new File(path.join(folder || '', ['asset', type, generateUniqueIdentifier(), 'json'].join('.')));

		file.write(xml);

		return file;
	}
};
