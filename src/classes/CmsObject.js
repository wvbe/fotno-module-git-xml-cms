const fs = require('fs-extra');
const path = require('path');

const ID = Symbol('id');
const FILE = Symbol('file content');

module.exports = class File {
	constructor (id) {
		if (!id)
			throw new Error('CMS object must have an id');

		this[ID] = id;
	}

	/**
	 *
	 * @returns {string}
	 */
	getLocation () {
		return path.join(process.cwd(), this[ID]);
	}

	/**
	 *
	 * @returns {string}
	 */
	getEncoding () {
		return 'utf-8';
	}

	/**
	 *
	 * @returns {boolean}
	 */
	exists () {
		return fs.existsSync(this.getLocation());
	}

	/**
	 * @returns {string}
	 */
	getId () {
		return this[ID];
	}

	getClass () {
		const basenameParts = path.basename(this.getLocation()).split('.');

		return basenameParts[0];
	}

	/**
	 *
	 * @returns {string}
	 */
	getType () {
		const basenameParts = path.basename(this.getLocation()).split('.');

		return basenameParts[1];
	}

	read (forceRefresh) {
		if (!this[FILE] || forceRefresh)
			this[FILE] = fs.readFileSync(this.getLocation(), this.getEncoding());

		return this[FILE];
	}

	/**
	 *
	 * @param {string} content
	 */
	write (content) {
		fs.ensureFileSync(this.getLocation());
		fs.writeFileSync(this.getLocation(), content, this.getEncoding());

		this[FILE] = content;
	}
};
