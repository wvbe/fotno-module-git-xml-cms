const fs = require('fs-extra');
const path = require('path');

const generateUniqueIdentifier = require('../util/generateUniqueIdentifier');

const CmsObject = require('./CmsObject');

module.exports = class Reference extends CmsObject {
	/**
	 *
	 * @returns {string}
	 */
	getType () {
		const basenameParts = path.basename(this.getLocation()).split('.');

		return basenameParts[0];
	}

	getObject (forceRefresh) {
		return Object.assign({
			permanentId: this.getId()
		}, JSON.parse(super.read(forceRefresh)));
	}

	/**
	 *
	 * @param {string} content
	 */
	write (content) {
		return super.write(JSON.stringify(content, null, '\t'));
	}

	/**
	 *
	 * @param {string} type
	 * @param {string} content
	 * @param {string} folder
	 * @returns {File}
	 */
	static create (type, target, metadata) {
		const reference = new Reference(['reference', type, generateUniqueIdentifier(), 'json'].join('.'));

		reference.write({
			type,
			target,
			metadata: metadata || null
		});

		return reference;
	}
};
