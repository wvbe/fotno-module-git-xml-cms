const fontoxpath = require('fontoxpath');

const REFERENCE_TYPES = {};
module.exports = {
	register: (name, query) => {
		REFERENCE_TYPES[name] = query;
	},
	getAll: () => REFERENCE_TYPES
};
