const globby = require('globby');

const File = require('./classes/File');
const Reference = require('./classes/Reference');

module.exports = {
	findFiles: () => globby(['**/asset.*.json'], { cwd: process.cwd() }).then(results => results.map(result => new File(result))),
	getFile:    (id) => new File(id),
	createFile: (type, content, folder) => File.create(type, content, folder),

	getReference: (id) => new Reference(id),
	createReference: (type, target, metadata) => Reference.create(type, target, metadata)
};
