const fontoxpath = require('fontoxpath');

const packageJson = require('../package.json');

const domToJsonml = require('./util/domToJsonml');
const stringToDom = require('./util/stringToDom');
const jsonmlToString = require('./util/jsonmlToString');

const DEFAULT_SERIALIZER = {
	serialize: (file, input, options) => {
		const dom = stringToDom(input);

		return JSON.stringify({
			title: fontoxpath.evaluateXPathToString('//title', dom) || null,
			generator: {
				name: packageJson.name,
				version: packageJson.version
			},
			dom: domToJsonml(dom)
		}, null, '\t');
	},
	deserialize: (file, input, options) => jsonmlToString(JSON.parse(input).dom)
};

const ASSET_TYPES = {};

module.exports = {
	register: (name, icon, serializer) => {
		ASSET_TYPES[name] = {
			icon,
			serializer: serializer || DEFAULT_SERIALIZER
		};
	},
	get: (name) => {
		if (!ASSET_TYPES[name])
			throw new Error(`Asset type "${name}" is not registered`);

		return ASSET_TYPES[name];
	}
};
