const assetTypes = require('./assetTypes');
const referenceTypes = require('./referenceTypes');

assetTypes.register(
	'document',
	'file-text-o');

assetTypes.register(
	'document-template',
	'file-o');

referenceTypes.register(
	'document',
	'self::*[name() = ("xref", "topicref")]/@href');
