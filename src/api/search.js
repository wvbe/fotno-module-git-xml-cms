const SearchIndex = require('search-index');
const path = require('path');

const repository = require('../repository');

let index = null;


function getIndex () {
	if (index) {
		return Promise.resolve(index);
	}
	return new Promise((resolve, reject) => SearchIndex(
		{
			indexPath: path.resolve(process.cwd(), '..', 'index'),
			preserveCase: false,
			storeable: true,
			searchable: true,
			fieldedSearch: true,
			nGramLength: [1, 2, 3]
		},
		(err, ind) => {
			if (err) {
				return reject(err);
			}

			index = ind;

			return resolve(index);
		}));
}

function getResults (term) {
	return getIndex()
		.then(index => {
			const results = [];
			return new Promise(resolve => index.search(term.toLowerCase().trim())
				.on('data', data => results.push(data))
				.on('end', () => resolve(results)))
				.then(results => results.map(result => repository.getFile(result.id)));
		});
}

module.exports = {
	getIndex: getIndex,
	getResults: getResults
};
