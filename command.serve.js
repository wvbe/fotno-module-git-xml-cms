'use strict';
const path = require('path'),
	express = require('express'),
	cors = require('cors');

const packageJson = require('./package.json');

const repository = require('./src/repository');

module.exports = (fotno, cmd) => {
	cmd.addCommand('serve')
		.setDescription('Serve an editor for your content repository.')

		.addOption(new fotno.Option('port')
			.setShort('p')
			.setDescription('Port number to serve on, defaults to 8079.')
			.isRequired(false)
			.setDefault(8079, true))

		.addOption(new fotno.Option('editor')
			.setShort('e')
			.setDescription('Path to the build of the editor you want to use. Defaults to a DITA Standard editor packaged with this module.')
			.isRequired(false)
			.setDefault(path.resolve(__dirname, '..', 'editor'), true))

		.setController((req, res) => {
			const server = express();

			server.use(cors());

			server.set('x-powered-by', `${packageJson.name} (v${packageJson.version}`);
			server.set('json spaces', 2);
			const config = {
				repo: process.cwd(), // must hardcode CWD unfortunately, because singleton File can not think of this shit itself
				editor: req.options.editor
			};

			const router = express.Router();

			[
				require('./src/middleware/editor'),
				require('./src/middleware/api')
			]
			.forEach(middleware => middleware(config, router, repository));

			server.use('/', router);

			res.caption('fotno git-cms serve');

			res.properties(Object.assign({}, config, {
				port: req.options.port
			}));

			server.listen(req.options.port, () => {
				res.success('Listening to port ' + req.options.port);
			});
		});
};
