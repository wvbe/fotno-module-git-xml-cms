module.exports = fotno => {

	const cmd = fotno.registerCommand('git-cms')
		.setDescription('Use a directory on your filesystem as a content repository.');

	[
		require('./command.search.js'),
		require('./command.serve.js'),
		require('./command.export.js'),
		require('./command.import-from-tabledata.js')
	].forEach(mod => mod(fotno, cmd));
};
