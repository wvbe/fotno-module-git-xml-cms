# fotno-module-cms-server

This module is intended to make version-controlling XML through git easy.

Will create a server for the DITA Standard application (unless the --editor flag is used) and expose the CMS standard
contracts. XML will be stored on the filesystem as JSON files containing a JSONML structure notation
and some extra metadata.

Type `fotno cms --help` for more information.
