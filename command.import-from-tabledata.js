'use strict';
const path = require('path'),
	os = require('os'),
	fontoxpath = require('fontoxpath'),
	express = require('express');

const repository = require('./src/repository');
const fsExtra = require('fs-extra');
const stringToDom = require('./src/util/stringToDom');
const domToJsonml = require('./src/util/domToJsonml');
const jsonmlToString = require('./src/util/jsonmlToString');
const tableDataToObjects = require('./src/util/tableDataToObjects');

const Reference = require('./src/classes/Reference');
const File = require('./src/classes/File');

module.exports = (fotno, cmd) => {
	cmd.addCommand('import-from-tabledata')
		.setDescription('Import stuff from a Lynkx TableData export')
		.addOption('nodes', 'n', 'Pointer to nodes.csv', true)
		.setController((req, res) => {
			res.caption('fotno git-cms import-from-tabledata');
			const timeStart = new Date().getTime();
			res.debug('Importing and sanitizing data dump...');
			const objects = tableDataToObjects(fsExtra.readFileSync(req.options.nodes, 'utf8'));

			let createdFiles = 0;
			res.debug('Creating files for documents and document templates');
			objects
				.filter(obj => ['Document', 'DocumentTemplate'].includes(obj.type))
				.forEach(obj => {
					const type = obj.type === 'DocumentTemplate'
							? 'document-template'
							: 'document',
						folder = obj.parentpath
							? obj.parentpath.replace(/[^a-z0-9/]/gi, '_').toLowerCase()
							: null;

					const file = new File(path.join(folder || '', ['asset', type, obj.guid, 'json'].join('.')));

					Object.assign(obj, {
						_file: file
					});

					++createdFiles;
				});


			let createdReferences = 0,
				skippedReferences = 0;
			res.debug('Converting references');
			res.indent();
			objects
				.filter(obj => obj.type === 'Reference')
				.forEach(obj => {
					let target = obj.extendedproperties.target,
						type = obj.extendedproperties.reftype;

					let targetObj = obj,
						i = 0;

					while (targetObj && targetObj.type === 'Reference') {
						if (targetObj.extendedproperties.reftype === 'web') {
							break;
						}

						targetObj = objects.find(o => o.guid === obj.extendedproperties.target);

						if(++i > 10) {
							throw new Error('Circular reference for reference ' + obj.extendedproperties.target);
						}
					}

					if (!targetObj) {
						res.debug('Skipping ' + type + ' reference ' + target + ' because it does not point to an existing document');
						++skippedReferences;
						return;
					}

					if (targetObj.type === 'Document') {
						target = targetObj._file.getId();
						type = 'document';
					}

					// Create a new Reference JSON
					const reference = new Reference(['reference', type, obj.guid, 'json'].join('.'));

					reference.write({
						type,
						target,
						metadata: {}
					});

					Object.assign(obj, {
						_reference: reference
					});

					++createdReferences;
				});
			res.outdent();


			console.log(objects);

			let convertedReferences = 0;
			res.debug('Splicing new permanentId\'s into XML');
			objects
				.filter(obj => ['Document', 'DocumentTemplate'].includes(obj.type))
				.forEach(obj => {
					const dom = stringToDom(obj.extendedproperties.content);

					fontoxpath.evaluateXPathToNodes('//*[@href]', dom)
						.forEach(node => {
							const oldTarget = node.getAttribute('href'),
								reference = objects.find(obj => obj.guid === oldTarget);

							if (!reference._reference) {
								return;
							}

							node.setAttribute('href', reference._reference.getId());

							++convertedReferences;
						});

					obj._file.write(jsonmlToString(domToJsonml(dom)));
				});

			res.break();

			res.success('Done');

			res.properties({
				'Time (seconds)': ((new Date().getTime()) - timeStart) / 1000,
				'XML documents': createdFiles,
				'Unique references': createdReferences,
				'Dead references': skippedReferences,
				'Reference occurrences': convertedReferences
			});
		});
};
