'use strict';
const path = require('path'),
	os = require('os'),
	fontoxpath = require('fontoxpath'),
	express = require('express');

const repository = require('./src/repository');
const fs = require('fs-extra');
const defaultConfiguration = require('./src/defaultConfiguration');
const stringToDom = require('./src/util/stringToDom');
const domToJsonml = require('./src/util/domToJsonml');
const jsonmlToString = require('./src/util/jsonmlToString');

module.exports = (fotno, cmd) => {
	cmd.addCommand('export')
		.setDescription('Exports your repository to plain XML.')
		.addOption(new fotno.Option('destination').setShort('d').setDescription('Destination for your exported XML').setDefault(path.join(process.cwd(), '_export'), true))
		.setController((req, res) => {
			res.caption('fotno git-cms export');
			res.debug('Indexing exportable files');
			return repository.findFiles()
				.then(files => {
					const index = files.reduce((index, file) => {
						const dom = stringToDom(file.getXml()),
							isMap = !!fontoxpath.evaluateXPathToNodes('//map', dom).length,
							originalLoc = path.join(
										path.relative(process.cwd(), path.dirname(file.getLocation())),
										file.getLabel().replace(/[^a-z0-9]/gi, '_').toLowerCase()),
							ext =  (isMap ? '.ditamap' : '.dita');

						let loc = originalLoc,
							i = 1;

						while (Object.keys(index).map(v => index[v]).includes(loc + ext)) {
							++i;
							loc = originalLoc + '-(' + i + ')';
						}

						return Object.assign(index, {
							[file.getId()]: loc + ext
						});
					}, {});


					files.forEach(file => {
						const dom = stringToDom(file.getXml()),
							newLocation = path.resolve(req.options.destination, index[file.getId()]);

						fontoxpath.evaluateXPathToNodes('//*[name(.) = ("xref", "topicref")]', dom)
							.forEach(node => {
								const reference = repository.getReference(node.getAttribute('href')),
									href = reference.getObject().target,
									hrefLeft = href.includes('#') ? href.substr(0, href.indexOf('#')) : href,
									hrefRight = href.includes('#') ? href.substr(href.indexOf('#')) : '';

								let newHref = href;

								if (reference.getObject().type === 'web') {
									newHref = newHref.replace(/&/g, '&amp;');
									newHref = encodeURI(newHref);
								}
								else {
									newHref = index[hrefLeft];

									newHref = path.relative(path.dirname(file.getLocation()), path.resolve(process.cwd(), newHref));
									if(newHref.charAt(0) !== '.')
										newHref = './' + newHref;

									newHref = newHref + hrefRight;
								}

								res.property(href, newHref);
								node.setAttribute('href', newHref);
							});

						fs.ensureFileSync(newLocation);
						fs.writeFileSync(
							newLocation,
							jsonmlToString(domToJsonml(dom)).replace(/urn:fontoxml:names:tc:dita:xsd/gi, 'urn:oasis:names:tc:dita:xsd') + os.EOL,
							'utf8');
					});
				})
				.catch(e => res.error(e.stack));
		});
};
