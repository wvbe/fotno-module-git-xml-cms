'use strict';
const path = require('path'),
	express = require('express'),
	cors = require('cors'),
	fontoxpath = require('fontoxpath');

const jsonmlToString = require('./src/util/jsonmlToString'),
	stringToDom = require('./src/util/stringToDom'),
	filterDom = require('./src/util/filterDom'),
	domToJsonml = require('./src/util/domToJsonml');

const getPublicDocumentObject = require('./src/middleware/publish/getPublicDocumentObject');

const fs = require('fs');
// Promisified instantiation of search-index
const search = require('./src/api/search');

const packageJson = require('./package.json');

const repository = require('./src/repository');

module.exports = (fotno, cmd) => {
	const indexOption = new fotno.Option('index').setShort('i').setDescription('Index file').setDefault('search-index', true);
	const searchCommand = cmd.addCommand('search')
		.setDescription('Search index tools.');

	searchCommand.addCommand('index')
		.addOption(indexOption)
		.setController((req, res) => {

			return Promise.all([
					repository.findFiles().then(x => {
						res.debug('Looked up ' + x.length + ' documents');
						return x;
					}),
					search.getIndex()
				])
				.then(results => {
					const files = results[0],
						index = results[1];

					// Find and prepare files to index
					const indexableFiles = files.map(file => {
						const dom = stringToDom(getPublicDocumentObject(file).dom),
							searchableText = fontoxpath.evaluateXPathToStrings('//text()', dom)
								.join(' ')
								.toLowerCase()
								.replace(/[^0-9a-z ]/g, '')
								.replace(/\t/g, ' ')
								.replace(/\n/g, ' ')
								.replace(/\s+/g,' ')
								.trim();

						return {
							id: file.getId(),
							body: searchableText
						};
					});

					// Build an index
					return new Promise((resolve, reject) => index.concurrentAdd({}, indexableFiles, err => err ? reject(err) : resolve()));
				})
				.then(index => {
					res.log('Saved index');
				});
	});
};
